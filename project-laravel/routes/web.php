<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);
Route::get('/register', [AuthController::class, 'register']);
Route::post('/welcome', [AuthController::class, 'welcome']);
Route::get('/data-table', function(){
    return view('page.data-table');
});
Route::get('/table', function(){
    return view('page.table');
});

// CRUD Cast

// Create Data
// Route untuk mengarah ke form tambah pemain film baru
Route::get('/cast/create', [CastController::class, 'create']);

// Route untuk menyimpan data pemain ke database
Route::post('/cast', [CastController::class, 'store']);

// Read
// untuk tampilkan semua data
Route::get('/cast', [CastController::class, 'index']);
// details cast berdasarkan id
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

// Update
// route mengararahkan ke form update berdasarkan id
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
// untuk update data berdasarkan id di database
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

// Delete
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);