@extends('layout.master')

@section('title')
Sanberbook
@endsection

@section('content')
<h1>Buat Account Baru!</h1>
<h2>Sign Up Form</h2>
<form action="/welcome" method="post">
    @csrf
    <label for="">First name:</label> <br> <br>
    <input type="text" name="first-name"> <br> <br>
    <label for="">Last name</label> <br> <br>
    <input type="text" name="last-name"> <br> <br>
    <label for="">Gender:</label> <br> <br>
    <input type="radio" name="gender-male"> Male <br>
    <input type="radio" name="gender-female"> Female <br>
    <input type="radio" name="gender-other"> Other <br> <br>
    <label for="">Nationality:</label> <br> <br>
    <select name="nationality" id="">
        <option value="indonesian">Indonesian</option>
        <option value="singaporean">Singaporean</option> 
        <option value="malaysians">Malaysians</option>
        <option value="australian">Australian</option>
    </select> <br> <br>
    <label for="">Language Spoken:</label> <br> <br>
    <input type="checkbox"> Bahasa Indonesia <br>
    <input type="checkbox"> English <br>
    <input type="checkbox"> Other <br> <br>
    <label for="">Bio:</label> <br> <br>
    <textarea name="bio" id="" cols="30" rows="10"></textarea> <br>
    <input type="submit" value="Sign Up">
</form> 
@endsection
