@extends('layout.master')

@section('title')
Halaman Detail Cast
@endsection

@section('content')

<h1 class="text-primary">{{$cast->nama}}</h1>
<p>{{$cast->umur}}</p>
<p>{{$cast->bio}}</p>
<a href="/cast" class="btn btn-danger my-2">Kembali</a>
@endsection